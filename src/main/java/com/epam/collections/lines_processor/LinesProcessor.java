/**
 * %W% %E% Aleksandr Kozlikhin
 * 
 * Task 09-generic-collections-lines-processor
 * 
 */
package com.epam.collections.lines_processor;

import com.epam.collections.lines_processor.figures.Line;
import com.epam.collections.lines_processor.figures.Point;
import com.epam.view.View;

import java.util.*;

/**
 * Class for working with inventory
 * 
 * @version 1.0 27 march 2020
 * @author Aleksandr Kozlikhin
 */
public class LinesProcessor {

    public static void main(String[] args) {
        View view = new View();
        view.start();
    }

    /**
     * Generate all available lines by list of points (with duplicates). Result
     * size should be equals to (n ^ 2) - n
     * 
     * @param points Points to generate lines
     * @return List of all available lines
     */
    public List<Line> mapPointsToLines(Iterable<Point> points) {
        List<Line> line = new ArrayList<>();
        Point[] point;
        point = readPoints(points);
        for (int i = View.NUMBER_ZERO; i < point.length
                - View.NUMBER_ONE; i++) {
            for (int j = i + View.NUMBER_ONE; j < point.length; j++) {
                Line line1 = new Line(point[i], point[j]);
                Line line2 = new Line(point[j], point[i]);
                line.add(line1);
                line.add(line2);
            }
        }
        return line;
    }

    /**
     * Reduce amount of lines - unite same lines build by different points in
     * one line
     * 
     * @param lines List of lines to be reduced
     * @return List of lines without duplicates
     */
    public List<Line> reduceLines(Iterable<Line> lines) {
        List<Line> line = new ArrayList<>();
        for (Iterator iterator = lines.iterator(); iterator.hasNext();) {
            Line line2 = (Line) iterator.next();
            line.add(line2);
        }
        Set<Line> setLine = new HashSet<>();
        setLine.addAll(line);
        line.clear();
        line.addAll(setLine);
        return line;
    }

    private Point[] readPoints(Iterable<Point> points) {
        int i = View.NUMBER_ZERO;
        for (Point point1 : points) {
            ++i;
        }
        Point[] point = new Point[i];
        i = View.NUMBER_ZERO;
        for (Point point1 : points) {
            point[i] = point1;
            ++i;
        }
        return point;
    }
}
