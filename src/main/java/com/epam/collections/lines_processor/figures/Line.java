package com.epam.collections.lines_processor.figures;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import com.epam.view.View;

/**
 * Class that stores lines data, represents by equation y = kx + b
 */
public class Line {

    private Set<Point> points = new HashSet<>();
    private double k;
    private double b;

    public Line(Point point1, Point point2) {
        this.k = (Double.valueOf(point2.getY()) - point1.getY())
                / (point2.getX() - point1.getX());
        if (!Double.isInfinite(k) && !Double.isNaN(k)) {
            k = new BigDecimal(k).setScale(View.NUMBER_TWO, RoundingMode.UP)
                    .doubleValue();
        }
        this.b = point1.getY() - k * point1.getX();
        if (!Double.isInfinite(b) && !Double.isNaN(b)) {
            b = new BigDecimal(b).setScale(View.NUMBER_TWO, RoundingMode.UP)
                    .doubleValue();
        }
        points.add(point1);
        points.add(point2);
    }

    public Set<Point> getPoints() {
        return points;
    }

    public double getK() {
        return k;
    }

    public double getB() {
        return b;
    }

    public void setPoints(Set<Point> points) {
        this.points = points;
    }

    @Override
    public int hashCode() {
        final int prime = View.NUMBER_THIRTY_ONE;
        int result = View.NUMBER_ONE;
        long temp;
        temp = Double.doubleToLongBits(b);
        result = prime * result
                + (int) (temp ^ (temp >>> View.NUMBER_THIRTY_TWO));
        temp = Double.doubleToLongBits(k);
        result = prime * result
                + (int) (temp ^ (temp >>> View.NUMBER_THIRTY_TWO));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Line other = (Line) obj;
        if (Double.doubleToLongBits(b) != Double.doubleToLongBits(other.b))
            return false;
        if (Double.doubleToLongBits(k) != Double.doubleToLongBits(other.k))
            return false;
        return true;
    }
}
