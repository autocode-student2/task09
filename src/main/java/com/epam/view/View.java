package com.epam.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.epam.collections.lines_processor.LinesProcessor;
import com.epam.collections.lines_processor.figures.Line;
import com.epam.collections.lines_processor.figures.Point;

public class View {

    public static final int NUMBER_ZERO = 0;
    public static final int NUMBER_ONE = 1;
    public static final int NUMBER_TWO = 2;
    public static final int NUMBER_THREE = 3;
    public static final int NUMBER_THIRTY_ONE = 31;
    public static final int NUMBER_THIRTY_TWO = 32;
    public static final String SYMBOL = ";";
    public static final String NEW_LINE = "\n";
    public static final String MESSAGE_INVALID_INPUT = "Invalid input!";
    public static final String MESSAGE_THERE_NUMBER = "there must be a number";
    public static final String MESSAGE_TO_FILE = "The line additionally passes through ";
    public static final String MESSAGE_POINT = " point(s)";
    public static final String MESSAGE_JOB_IS_DONE = "the job is done";
    public static final String ERROR_IN_OUT = "Error input/output: ";
    public static final String ORIGINAL_FILE = "src\\main\\resources\\points.txt";
    public static final String RESULT_FILE = "src\\main\\resources\\result.txt";

    public void start() {
        List<Point> point = new ArrayList<>();
        List<Line> line;
        try {
            inputFile(point);
            LinesProcessor linesProcessor = new LinesProcessor();
            line = linesProcessor.mapPointsToLines(point);
            line = linesProcessor.reduceLines(line);
            checkingWhetherPointFitsIntoLine(line, point);
            checkingLineForAdditionalPoints(line);
        } catch (IOException e) {
            System.out.println(ERROR_IN_OUT + e.getMessage());
        }
        System.out.print(MESSAGE_JOB_IS_DONE);
    }

    private List<Point> inputFile(List<Point> point) throws IOException {
        File file = new File(ORIGINAL_FILE);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String s;
            while ((s = br.readLine()) != null) {
                int x = checkingForNumber(
                        s.substring(NUMBER_ZERO, s.indexOf(SYMBOL)));
                int y = checkingForNumber(
                        s.substring(s.indexOf(SYMBOL) + NUMBER_ONE));
                point.add(new Point(x, y));
            }
        }
        return point;
    }

    private int checkingForNumber(String code) {
        int number = NUMBER_ZERO;
        try {
            number = Integer.parseInt(code);
        } catch (NumberFormatException e) {
            System.out.println(MESSAGE_INVALID_INPUT);
            System.out.println(MESSAGE_THERE_NUMBER);
        }
        return number;
    }

    private void checkingWhetherPointFitsIntoLine(List<Line> line,
            List<Point> point) {
        for (Iterator iterator = line.iterator(); iterator.hasNext();) {
            Line line2 = (Line) iterator.next();
            equalsPoints(line2, point);
        }
    }

    private void equalsPoints(Line line, List<Point> point) {
        Set<Point> pointsLine = line.getPoints();
        Point[] pointLine = new Point[pointsLine.size()];
        int i = NUMBER_ZERO;
        for (Point point2 : pointsLine) {
            pointLine[i] = point2;
            ++i;
        }
        for (Point point2 : point) {
            if (!point2.equals(pointLine[NUMBER_ZERO])
                    && !point2.equals(pointLine[NUMBER_ONE])) {
                checkingWhetherPointBelongsToLine(line, point2, pointsLine);
            }
        }
    }

    private void checkingWhetherPointBelongsToLine(Line line, Point point,
            Set<Point> pointsLine) {
        if (point.getY() == (int) line.getK() * point.getX() + line.getB()) {
            pointsLine.add(point);
            line.setPoints(pointsLine);
        }
    }

    private void checkingLineForAdditionalPoints(List<Line> line) {
        List<Line> outputLine = new ArrayList<>();
        for (Line line2 : line) {
            if (line2.getPoints().size() >= NUMBER_THREE) {
                outputLine.add(line2);
            }
        }
        outputStringToFile(preparingStringToWriteToFile(outputLine));
    }

    private String preparingStringToWriteToFile(List<Line> outputLine) {
        StringBuilder s = new StringBuilder();
        for (Line line : outputLine) {
            s.append(MESSAGE_TO_FILE);
            s.append(Integer.valueOf(line.getPoints().size() - NUMBER_TWO));
            s.append(MESSAGE_POINT);
            s.append(NEW_LINE);
        }
        return s.toString();
    }

    private void outputStringToFile(String s) {
        try (FileWriter fw = new FileWriter(RESULT_FILE)) {
            fw.write(s);
        } catch (IOException e) {
            System.out.println(ERROR_IN_OUT + e.getMessage());
        }
        System.out.println(s);
    }
}
